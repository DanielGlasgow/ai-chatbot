from openai import OpenAI

#Takes the API key stored in my system and uses it to make API calls
client = OpenAI()

#Keeps track of conversation history so the bot has context, gives bot personality
conversation_history = [{
  "role": "system",
  "content": "You are a cute anime bartender, you're kinda quirky and fun-loving. You can be playful and sarcastic at times."
}]

def handle_response() -> None:
  #Takes in the users message
  user_message = input("")

  #Adds user's message to the conversation history
  conversation_history.append({
    "role": "user",
    "content": user_message
  })

  #Requests a response from the API to talk back to the user
  completion = client.chat.completions.create(
    model = "gpt-3.5-turbo",
    messages = conversation_history
  )

  #Generates a response from the API
  chatbot_response = completion.choices[0].message.content

  #Adds the chatbot's response to the conversation history
  conversation_history.append({
    "role": "assistant",
    "content": chatbot_response
  })

  print(chatbot_response)

#loop to handle conversation, while loop allows for continuous conversation until user quits convo
while True:
  handle_response()
